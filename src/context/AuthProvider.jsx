import { useState, useEffect, createContext } from 'react'

const AuthContext = createContext()

// eslint-disable-next-line react/prop-types
const AuthProvider = ({ children }) => {

    const [auth, setAuth] = useState({})

    useEffect(() => {
        const autenticarUsuario = async () => {
            const token = localStorage.getItem('Token')

            if (!token) return

            const config = {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`
                }
            }

            try {
                // eslint-disable-next-line no-undef
                const { data } = await clienteAxios('/veterinarios/perfil', config)

                setAuth(data)
            } catch (error) {
                console.log(error.response.data.msg);
                setAuth({})
            }
        }
        autenticarUsuario()
    }, [])


    return (
        <AuthContext.Provider
            value={{
                auth,
                setAuth
            }}
        >
            {children}
        </AuthContext.Provider>
    )
}

export {
    AuthProvider
}

export default AuthProvider