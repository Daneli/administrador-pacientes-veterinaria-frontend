import { Outlet } from "react-router-dom"
import Header from "../components/Header"
import Footer from "../components/Footer"

// import useAuth from "../hooks/useAuth"

const RutaProtegida = () => {

  // const { auth } = useAuth()
  // console.log(auth);

  return (
    <>
      {/* {auth ? <Outlet /> : <Navigate to="/" />} */}
      <Header />
      <main className="container mx-auto mt-10">
        <Outlet />
      </main>
      <Footer />
    </>
  )
}

export default RutaProtegida