import { BrowserRouter, Route, Routes } from "react-router-dom"
import AuthLayout from "./layout/AuthLayout"
import RutaProtegida from "./Layout/RutaProtegida"

import Login from "./views/Login"
import ConfirmarCuenta from "./views/ConfirmarCuenta"
import Registrar from "./views/Registrar"
import OlvidePassword from "./views/OlvidePassword"
import NuevaContraseña from "./views/NuevaContraseña"
// import AuthProvider from "./context/AuthProvider"
import AdministrarPacientes from "./views/AdministrarPacientes"
import Perfil from "./views/Perfil"
import CambiarContraseña from "./views/CambiarContraseña"

function App() {

  return (
    // <AuthProvider>
    <BrowserRouter>
      <Routes>
        {/* Layout es el componente padre que todos los componentes tendrán, por ello se agrega dentro de doble etiqueta y las demás se cierran con / */}
        <Route path="/" element={<AuthLayout />}>
          <Route element={<Login />} index />
          <Route path="confirmarcuenta/:id" element={<ConfirmarCuenta />} index />
          <Route path="registrar" element={<Registrar />} index />
          <Route path="olvidepassword" element={<OlvidePassword />} index />
          <Route path="nuevopassword" element={<NuevaContraseña />} index />
        </Route >

        <Route path="/admin" element={<RutaProtegida />}>
          <Route element={<AdministrarPacientes />} index />
          <Route path="perfil" element={<Perfil />} index />
          <Route path="cambiarpassword" element={<CambiarContraseña />} index />
        </Route>

      </Routes>
    </BrowserRouter>
    // </AuthProvider>

  )
}

export default App
