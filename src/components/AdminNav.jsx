import { Link } from "react-router-dom"

const AdminNav = () => {
  return (
    <nav>
      <Link to='/admin/perfil' className='font-bold uppercase text-gray-500 px-5'>
        Perfil
      </Link>
      <Link to='/admin/cambiarpassword' className='font-bold uppercase text-gray-500 px-5'>
        Cambiar contraseña
      </Link>
    </nav>
  )
}

export default AdminNav