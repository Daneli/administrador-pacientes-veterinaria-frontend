import Paciente from "./Paciente"

const ListadoPacientes = () => {
  return (
    <>
      <h2 className="font-black text-3xl text-center">Listado de Pacientes</h2>
      <p className="text-xl mt-5 mb-10 text-center">
        Administra tus {""}
        <span className="text-indigo-600 font-bold">
          Pacientes y Citas
        </span>
      </p>
      <Paciente />
    </>
  )
}

export default ListadoPacientes