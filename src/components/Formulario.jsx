import { useState } from "react";

const Formulario = () => {

  const [nombre, setNombre] = useState('')
  const [propietario, setPropietario] = useState('')
  const [email, setEmail] = useState('')
  const [fecha, setFecha] = useState(Date.now())
  const [sintomas, setSintomas] = useState('')

  const handleSubmit = (e) => {
    e.preventDefault()

    // Validar formulario
    if ([nombre, propietario, email, fecha, sintomas].includes('')) {
      console.log('Todos los campos son obligatorios');
    }
  }

  return (
    <>
      <p className="text-lg text-center mb-10">
        Añade tus Pacientes y {""}
        <span className="text-indigo-600 font-bold">Administrarlos</span>
      </p>
      <form onSubmit={handleSubmit} className="bg-white py-10 px-5 rounded-lg">
        <div className="mb-5">
          <label
            htmlFor="nombreMascota"
            className="text-gray-700 uppercase font-semibold"
          >
            Nombre Mascota
          </label>
          <input
            type="text"
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            name="nombreMascota"
            id="nombreMascota"
            placeholder="Nombre de la Mascota"
            value={nombre}
            onChange={e => setNombre(e.target.value)}
          />
        </div>

        <div className="mb-5">
          <label
            htmlFor="propietario"
            className="text-gray-700 uppercase font-semibold"
          >
            Propietario
          </label>
          <input
            type="text"
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            name="propietario"
            id="propietario"
            placeholder="Propietario"
            value={propietario}
            onChange={e => setPropietario(e.target.value)}
          />
        </div>

        <div className="mb-5">
          <label
            htmlFor="email"
            className="text-gray-700 uppercase font-semibold"
          >
            Correo electrónico
          </label>
          <input
            type="email"
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            name="email"
            id="email"
            placeholder="Correo electrónico"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </div>

        <div className="mb-5">
          <label
            htmlFor="fecha"
            className="text-gray-700 uppercase font-semibold"
          >
            Fecha de Alta
          </label>
          <input
            type="date"
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            name="fecha"
            id="fecha"
            placeholder="Fecha de Alta"
            value={fecha}
            onChange={e => setFecha(e.target.value)}
          />
        </div>

        <div className="mb-5">
          <label
            htmlFor="sintomas"
            className="text-gray-700 uppercase font-semibold"
          >
            Síntomas
          </label>
          <textarea
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            id="sintomas"
            placeholder="Describe los síntomas"
            value={sintomas}
            onChange={e => setSintomas(e.target.value)}
          />
        </div>

        <input
          type="submit"
          value="Agregar pacientes"
          className="bg-indigo-600 w-full p-3 text-white uppercase font-semibold hover:bg-indigo-700 cursor-pointer transition-colors rounded-lg"
        />
      </form>
    </>
  );
};

export default Formulario;
