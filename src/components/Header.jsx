import { Link } from "react-router-dom"

const Header = () => {
    return (
        <header className="py-10 bg-indigo-600">
            <div className="container mx-auto flex justify-between items-center flex-col lg:flex-row">
                <h1 className="font-bold text-2xl text-indigo-200">Administrador de Pacientes {""}
                    <span className="text-white">
                        Veterinaria
                    </span>
                </h1>

                <nav className="gap-4 flex flex-col lg:flex-row items-center mt-5 lg:mt-0">
                    <Link to='/admin' className="text-white text-sm uppercase font-semibold">Pacientes</Link>
                    <Link to='/admin' className="text-white text-sm uppercase font-semibold">Perfil</Link>

                    <button type="button" className="text-white text-sm uppercase font-semibold">Cerrar sesión</button>
                </nav>
            </div>
        </header>
    )
}

export default Header