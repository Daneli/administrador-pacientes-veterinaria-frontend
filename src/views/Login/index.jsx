import { useState } from "react";
import { Link } from "react-router-dom";
// import useAuth from "../../hooks/useAuth";
import Alerta from "../../components/Alerta";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [alerta, setAlerta] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault()

    if ([email, password].includes('')) {
      setAlerta('Todos los campos son obligatorios')
    }
    return
  }

  return (
    <>
      <div>
        <h1 className="text-indigo-600 font-black text-6xl">
          Inicia sesión y Administra tus {""}
          <span className="text-black">pacientes</span>
        </h1>
      </div>
      <div className="mt-20 md:mt-5 shadow-lg px-5 py-10 rounded-xl bg-white">
        <Alerta alerta={alerta} />
        <form onSubmit={handleSubmit} action="">
          <div className="my-5">
            <label htmlFor="email">
              <input
                id="email"
                name="email"
                type="email"
                placeholder="ejemplo@correo.com"
                className="border w-full p-3 mt-3 bg-gray-50 rounded-xl "
                value={email}
                onChange={e => setEmail(e.target.value)}
              />
            </label>
          </div>

          <div className="my-5">
            <label htmlFor="password">
              <input
                id="password"
                name="password"
                type="password"
                placeholder="********"
                className="border w-full p-3 mt-3 bg-gray-50 rounded-xl "
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
            </label>
          </div>

          <input
            type="submit"
            value="Iniciar Sesión"
            className="bg-indigo-700 w-full py-3 px-10 rounded-xl text-white uppercase font-semibold mt-5 hover:cursor-pointer hover:bg-indigo-800 lg:w-auto"
          />
        </form>

        <nav className="mt-10 flex flex-col items-center justify-center lg:flex lg:flex-row lg:justify-between text-gray-500 text-sm">
          <Link to="/registrar">¿No tienes una cuenta?, Regístrate</Link>
          <Link to="/olvidepassword" className="mt-2">
            Olvidé mi contraseña
          </Link>
        </nav>
      </div>
    </>
  );
};

export default Login;
