import { useState } from "react";
import { Link } from "react-router-dom";
// import axios from "axios";
import Alerta from "../../components/Alerta";

const Registrar = () => {
  const [nombre, setNombre] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmar, setPasswordConfirmar] = useState("");
  const [alerta, setAlerta] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault()

    if ([nombre, email, password, passwordConfirmar].includes('')) {
      setAlerta('Existen campos vacíos');
      return
    }

    if (password !== passwordConfirmar) {
      setAlerta('Las contraseñas no son iguales');
      return
    }

    if (password.length < 8) {
      setAlerta('Password debe tener ocho caracteres');
      return
    }

    setAlerta({})

    // try {
    //   const url = "http://localhost:4000/api/veterinario"
    //   const respuesta = await axios.get(url, {nombre, email, password})
    //   return respuesta.data
    // } catch (error) {
    //   console.log(error);
    // }

  }

  return (
    <>
      <div>
        {/* Crear un espacio se usa {""} con react */}
        <h1 className="text-indigo-600 font-black text-6xl">
          Crea tu Cuenta y Administra {""}
          <span className="text-black">tus pacientes</span>
        </h1>
      </div>
      <div className="mt-20 md:mt-5 shadow-lg px-5 py-10 rounded-xl bg-white">

        <Alerta alerta={alerta} />

        <form onSubmit={handleSubmit} action="">
          <div className="my-5">
            <label htmlFor="nombre" className="text-gray-600 block">
              <input
                id="nombre"
                name="nombre"
                type="text"
                placeholder="Nombre"
                className="border w-full p-3 mt-3 bg-gray-50 rounded-xl "
                value={nombre}
                onChange={e => setNombre(e.target.value)}
              />
            </label>
          </div>

          <div className="my-5">
            <label htmlFor="email" className="text-gray-600">
              <input
                id="email"
                name="email"
                type="email"
                placeholder="Email"
                className="border w-full p-3 mt-3 bg-gray-50 rounded-xl "
                value={email}
                onChange={e => setEmail(e.target.value)}
              />
            </label>
          </div>

          <div className="my-5">
            <label htmlFor="password" className="text-gray-600">
              <input
                id="password"
                name="password"
                type="password"
                placeholder="Contraseña"
                className="border w-full p-3 mt-3 bg-gray-50 rounded-xl "
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
            </label>
          </div>

          <div className="my-5">
            <label htmlFor="passwordConfirmar" className="text-gray-600">
              <input
                id="passwordConfirmar"
                name="passwordConfirmar"
                type="passwordConfirmar"
                placeholder="Confirmar contraseña"
                className="border w-full p-3 mt-3 bg-gray-50 rounded-xl "
                value={passwordConfirmar}
                onChange={e => setPasswordConfirmar(e.target.value)}
              />
            </label>
          </div>

          <input
            type="submit"
            value="Crear cuenta"
            className="bg-indigo-700 w-full py-3 px-10 rounded-xl text-white uppercase font-semibold mt-5 hover:cursor-pointer hover:bg-indigo-800 lg:w-auto"
          />
        </form>

        <nav className="mt-10 flex flex-col items-center justify-center lg:flex lg:flex-row lg:justify-between text-gray-500 text-sm">
          <Link to="/">¿Ya tienes una cuenta?, Inicia sesión</Link>
          <Link to="/" className="mt-2">
            Olvidé mi contraseña
          </Link>
        </nav>
      </div>
    </>
  );
};

export default Registrar;
