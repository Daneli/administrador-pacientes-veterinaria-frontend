import AdminNav from "../../components/AdminNav"

const CambiarContraseña = () => {
  return (
    <>
      <AdminNav />

      <h2 className="font-black text-3xl text-center mt-10">Cambiar Contraseña</h2>
      <p className="text-xl mt-5 mb-10 text-center">Modifica tu {""}
        <span className="text-indigo-600 font-bold">Password aquí</span>
      </p>

      <div className="flex justify-center">
        <div className="w-full md:w-1/2 bg-white shadow rounded-lg p-5">
          <form action="">
            <div className="my-3">
              <label className="text-gray-600" htmlFor="passwordActual">
                <input id="passwordActual" type="password" className="border bg-gray-50 w-full p-2 mt-5 rounded-lg" name="Actual" placeholder="Contraseña Actual" />
              </label>
            </div>

            <div className="my-3">
              <label className="text-gray-600" htmlFor="passwordNuevo">
                <input id="passwordNuevo" type="password" className="border bg-gray-50 w-full p-2 mt-5 rounded-lg" name="passwordNuevo" placeholder="Nueva Contraseña" />
              </label>
            </div>
            <input type="submit" value="Guardar Cambios" className="bg-indigo-700 px-10 py-3 font-bold text-white rounded-lg uppercase w-full mt-5" />
          </form>
        </div>
      </div>
    </>
  )
}

export default CambiarContraseña