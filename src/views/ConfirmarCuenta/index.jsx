import { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";
import Alerta from "../../components/Alerta";

const ConfirmarCuenta = () => {
  const [email, setEmail] = useState("");
  const [alerta, setAlerta] = useState("");

  const handleSubmit = async () => {
    email.preventDefault();

    if (email === "") {
      setAlerta("Email es requerido");
      return;
    }
  };

  const params = useParams();
  const { id } = params;

  useEffect(() => {
    const confirmarCuenta = async () => {
      try {
        const url = `https://localhost:4000/api/veterinario/confirmar/${id}`;
        const { data } = await axios(url);
        return data;
      } catch (error) {
        console.log(error);
      }
    };
    confirmarCuenta();
  }, [id]);

  return (
    <>
      <div>
        {/* Crear un espacio se usa {""} con react */}
        <h1 className="text-indigo-600 font-black text-6xl">
          Crea tu Cuenta y Administra {""}
          <span className="text-black">tus pacientes</span>
        </h1>
      </div>
      <div className="mt-20 md:mt-5 shadow-lg px-5 py-10 rounded-xl bg-white">
        <Alerta alerta={alerta} />
        <form onSubmit={handleSubmit} action="">
          <div className="my-5">
            <label htmlFor="email">
              <input
                id="email"
                name="email"
                type="email"
                placeholder="ejemplo@correo.com"
                className="border w-full p-3 mt-3 bg-gray-50 rounded-xl "
                value={email}
                onChange={e => setEmail(e.target.value)}
              />
            </label>
          </div>

          <input
            type="submit"
            value="Enviar instrucciones"
            className="bg-indigo-700 w-full py-3 px-10 rounded-xl text-white uppercase font-semibold mt-5 hover:cursor-pointer hover:bg-indigo-800 lg:w-auto"
          />
        </form>
        <nav className="mt-10 flex flex-col items-center justify-center lg:flex lg:flex-row lg:justify-between text-gray-500 text-sm">
          <Link to="/registrar">¿No tienes una cuenta?, Regístrate</Link>
          <Link to="/olvidepassword" className="mt-2">
            ¿Ya tienes una cuenta?, Inicia sesión
          </Link>
        </nav>
      </div>
    </>
  );
};

export default ConfirmarCuenta;
