import { useState } from "react"
import { Link } from "react-router-dom"

const NuevaContraseña = () => {
    const [passwordNew, setPasswordNew] = useState('')

    return (
        <>
            <div>
                {/* Crear un espacio se usa {""} con react */}
                <h1 className="text-indigo-600 font-black text-6xl">
                    Crea tu Cuenta y Administra {""}
                    <span className="text-black">tus pacientes</span>
                </h1>
            </div>
            <div className="mt-20 md:mt-5 shadow-lg px-5 py-10 rounded-xl bg-white">

                <form action="">
                    <div className="my-5">
                        <label htmlFor="passwordNuevo">
                            <input
                                id="passwordNuevo"
                                name="passwordNuevo"
                                type="password"
                                placeholder="********"
                                className="border w-full p-3 mt-3 bg-gray-50 rounded-xl "
                                value={passwordNew}
                                onChange={e => setPasswordNew(e.target.value)}
                            />
                        </label>
                    </div>

                    <input
                        type="submit"
                        value="Guardar"
                        className="bg-indigo-700 w-full py-3 px-10 rounded-xl text-white uppercase font-semibold mt-5 hover:cursor-pointer hover:bg-indigo-800 lg:w-auto"
                    />
                </form>
                <nav className="mt-10 flex flex-col items-center justify-center lg:flex lg:flex-row lg:justify-between text-gray-500 text-sm">
                    <Link to="/registrar">¿No tienes una cuenta?, Regístrate</Link>
                    <Link to="/olvidepassword" className="mt-2">
                        ¿Ya tienes una cuenta?, Inicia sesión
                    </Link>
                </nav>
            </div>
        </>
    )
}

export default NuevaContraseña